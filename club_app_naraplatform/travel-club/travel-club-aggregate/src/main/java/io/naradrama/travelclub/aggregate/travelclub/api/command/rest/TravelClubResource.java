package io.naradrama.travelclub.aggregate.travelclub.api.command.rest;

import io.naradrama.travelclub.aggregate.travelclub.api.command.command.MemberCommand;
import io.naradrama.travelclub.aggregate.travelclub.api.command.command.TravelClubCommand;
import io.naradrama.travelclub.aggregate.travelclub.api.command.facade.TravelClubFacade;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import io.naradrama.travelclub.aggregate.travelclub.domain.logic.MemberLogic;
import io.naradrama.travelclub.aggregate.travelclub.domain.logic.TravelClubLogic;
import io.naradrama.travelclub.aggregate.travelclub.store.MemberClubStore;
import io.naradrama.travelclub.aggregate.travelclub.store.TravelClubStore;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.TravelClubMongoStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/aggregate/travel")
public class TravelClubResource implements TravelClubFacade {

    private final TravelClubLogic travelClubLogic;
    private final MemberLogic memberLogic;

    public TravelClubResource(TravelClubLogic travelClubLogic, MemberLogic memberLogic) {
        this.travelClubLogic = travelClubLogic;
        this.memberLogic = memberLogic;
    }

    @Override
    @PostMapping("/club/command")
    public TravelClubCommand executedTravelClub(TravelClubCommand travelClubCommand) {
        //
        return travelClubLogic.routeCommand(travelClubCommand);
    }

    @Override
    @PostMapping("/member/command")
    public MemberCommand executedMemberClub(@RequestBody MemberCommand memberCommand) throws NoSuchFieldException {
        //
        return memberLogic.routeCommand(memberCommand);
    }







    /*
    @GetMapping("/all")
    public ResponseEntity<List<TravelClub>> getAllClubs(){
        return ResponseEntity.ok().body(travelClubService.getAllClub());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TravelClub> getTravelClubId(@PathVariable String id){
        return ResponseEntity.ok().body(travelClubService.getTravelClubById(id));
    }

    @PostMapping("/add")
    public ResponseEntity<TravelClub> createTravelClub(@RequestBody TravelClub travelClub){
        return ResponseEntity.ok().body(travelClubService.createTravelClub(travelClub));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity <TravelClub> updateTravelClub(@PathVariable String id, @RequestBody TravelClub travelClub) {
        travelClub.setId(id);
        return ResponseEntity.ok().body(this.travelClubService.updateTravelClub(travelClub));
    }

    @DeleteMapping("/delete/{id}")
    public HttpStatus deleteClub(@PathVariable String id){
        this.travelClubService.deleteTravelClub(id);

        return HttpStatus.OK;
    }

    */

}
