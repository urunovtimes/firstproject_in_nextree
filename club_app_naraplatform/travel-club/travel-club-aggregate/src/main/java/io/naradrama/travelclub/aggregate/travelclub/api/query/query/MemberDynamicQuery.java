package io.naradrama.travelclub.aggregate.travelclub.api.query.query;

import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import io.naradrama.prologue.util.query.DocQueryBuilder;
import io.naradrama.prologue.util.query.DocQueryRequest;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.document.MemberDoc;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.query.Query;

@Getter
@Setter
@NoArgsConstructor
public class MemberDynamicQuery extends CqrsDynamicQuery <Membership>{
    //
    public void execute(DocQueryRequest<MemberDoc> request){
        //
        request.addQueryStringAndClass(genSqlString(), MemberDoc.class);
        request.addCollectionName("Membership");
        Query query = DocQueryBuilder.build(request);
        MemberDoc memberDoc = request.findOne(query);
        setQueryResult(memberDoc.toDomain());
    }
}
