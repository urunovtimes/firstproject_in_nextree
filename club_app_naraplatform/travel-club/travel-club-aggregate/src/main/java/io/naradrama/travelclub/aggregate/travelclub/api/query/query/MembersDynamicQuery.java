package io.naradrama.travelclub.aggregate.travelclub.api.query.query;

import io.naradrama.prologue.domain.Offset;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import io.naradrama.prologue.util.query.DocQueryBuilder;
import io.naradrama.prologue.util.query.DocQueryRequest;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.document.MemberDoc;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
public class MembersDynamicQuery extends CqrsDynamicQuery<List<Membership>> {
    //
    public void execute(DocQueryRequest<MemberDoc> request){
        //
        request.addQueryStringAndClass(genSqlString(), MemberDoc.class);
        request.addCollectionName("Membership");
        Query query = DocQueryBuilder.build(request, getOffset());
        List<MemberDoc> memberDocs = request.findAll(query);
        setQueryResult(MemberDoc.toDomains(memberDocs));
        if(nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            long totalCount = request.count(query);
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount) ;
        }
    }
}
