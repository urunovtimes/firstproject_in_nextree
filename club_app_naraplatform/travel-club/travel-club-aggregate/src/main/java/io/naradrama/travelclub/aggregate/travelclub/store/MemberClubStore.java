package io.naradrama.travelclub.aggregate.travelclub.store;

import io.naradrama.prologue.domain.Offset;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;

import java.util.List;

public interface MemberClubStore {
    //
    void create(Membership membership);
    Membership retrieve(String id);
    List<Membership> retrieveAll(Offset offset);
    void update(Membership membership);
    void delete(Membership membership);
    void delete(String id);
}
