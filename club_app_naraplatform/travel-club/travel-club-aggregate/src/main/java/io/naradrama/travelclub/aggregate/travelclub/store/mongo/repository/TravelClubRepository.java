package io.naradrama.travelclub.aggregate.travelclub.store.mongo.repository;

import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.document.TravelClubDoc;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface TravelClubRepository extends MongoRepository<TravelClubDoc, String> {
}
