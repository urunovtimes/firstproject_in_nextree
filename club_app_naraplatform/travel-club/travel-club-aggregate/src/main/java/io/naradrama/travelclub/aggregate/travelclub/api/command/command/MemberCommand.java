package io.naradrama.travelclub.aggregate.travelclub.api.command.command;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.sdo.MemberCdo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MemberCommand extends CqrsBaseCommand {
    //
    private String id;
    private NameValueList valueList;
    private MemberCdo memberCdo;

    protected MemberCommand(CqrsBaseCommandType type) {
        //
        super(type);
    }

    public static MemberCommand newRegisterMemberCommand(MemberCdo memberCdo){
        //
        MemberCommand command = new MemberCommand(CqrsBaseCommandType.Register);
        command.setMemberCdo(memberCdo);
        return command;
    }

    public static MemberCommand newModifiyMemberCommand(String id, NameValueList nameValueList){
        //
        MemberCommand memberCommand = new MemberCommand(CqrsBaseCommandType.Modify);
        memberCommand.setId(id);
        memberCommand.setValueList(nameValueList);
        return memberCommand;
    }

    public static MemberCommand newRemoveMemberCommand(String id, NameValueList nameValueList){
        //
        MemberCommand command = new MemberCommand(CqrsBaseCommandType.Remove);
        command.setId(id);
        return command;
    }

    public String toString(){
        //
        return toJson();
    }
    public static MemberCommand fromJson(String json){
        //
        return JsonUtil.fromJson(json, MemberCommand.class);
    }
}
