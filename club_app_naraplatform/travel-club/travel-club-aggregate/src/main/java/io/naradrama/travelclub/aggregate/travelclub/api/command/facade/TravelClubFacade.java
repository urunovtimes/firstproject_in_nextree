package io.naradrama.travelclub.aggregate.travelclub.api.command.facade;

import io.naradrama.travelclub.aggregate.travelclub.api.command.command.MemberCommand;
import io.naradrama.travelclub.aggregate.travelclub.api.command.command.TravelClubCommand;

public interface TravelClubFacade {
    //
    TravelClubCommand executedTravelClub(TravelClubCommand travelClubCommand);
    MemberCommand executedMemberClub(MemberCommand memberCommand) throws NoSuchFieldException;
}
