package io.naradrama.travelclub.aggregate.travelclub.store;

import io.naradrama.prologue.domain.Offset;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;

import java.util.List;

public interface TravelClubStore {

    void create(TravelClub travelClub);
    TravelClub retrieve(String id);
    void update(TravelClub travelClub);
    void delete(TravelClub travelClub);
    void delete(String id);
    List<TravelClub> retrieveAll(Offset offset);

}
