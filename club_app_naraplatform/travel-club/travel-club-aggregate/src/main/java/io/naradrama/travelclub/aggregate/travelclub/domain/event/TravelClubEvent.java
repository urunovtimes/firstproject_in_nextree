package io.naradrama.travelclub.aggregate.travelclub.domain.event;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravelClubEvent extends CqrsDataEvent {
    //
    private TravelClub travelClub;
    private String clubId;
    private NameValueList nameValueList;

    public TravelClubEvent(CqrsDataEventType cqrsDataEventType, CommandIdentity commandIdentity) {
        //
        super(cqrsDataEventType, commandIdentity);
    }

    public static TravelClubEvent newTravelClubRegisterEvent(TravelClub travelClub, CommandIdentity commandIdentity) {
        //
        TravelClubEvent clubEvent = new TravelClubEvent(CqrsDataEventType.Registered, commandIdentity);
        clubEvent.setTravelClub(travelClub);
        return clubEvent;
    }

    public static TravelClubEvent newTravelClubModifiedEvent(String clubId, NameValueList nameValue, CommandIdentity commandIdentity) {
        //
        TravelClubEvent clubEvent = new TravelClubEvent(CqrsDataEventType.Modified, commandIdentity);
        clubEvent.setClubId(clubId);
        clubEvent.setNameValueList(nameValue);
        return clubEvent;
    }

    public static TravelClubEvent newTravelClubModifiedEvent(TravelClub travelClub, CommandIdentity commandIdentity){
        //
        TravelClubEvent clubEvent = new TravelClubEvent(CqrsDataEventType.Modified, commandIdentity);
        clubEvent.setClubId(travelClub.getId());
        clubEvent.setTravelClub(travelClub);
        return clubEvent;
    }

    public static TravelClubEvent newTravelClubRemoveEvent(String id, CommandIdentity commandIdentity){
        //
        TravelClubEvent clubEvent = new TravelClubEvent(CqrsDataEventType.Removed, commandIdentity);
        clubEvent.setClubId(id);
        return clubEvent;
    }


    public String toString() {
        return toJson();
    }

    public static TravelClubEvent fromJson(String json) {
        //
        return JsonUtil.fromJson(json, TravelClubEvent.class);
    }
}
