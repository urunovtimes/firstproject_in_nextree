package io.naradrama.travelclub.aggregate.travelclub.store.mongo;

import io.naradrama.prologue.domain.Offset;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import io.naradrama.travelclub.aggregate.travelclub.store.MemberClubStore;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.document.MemberDoc;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.repository.MemberRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class MemberMongoStore implements MemberClubStore {
    //
    private final MemberRepository memberRepository;

    public MemberMongoStore(MemberRepository memberRepository) {
        //
        this.memberRepository = memberRepository;
    }

    @Override
    public void create(Membership membership) {
        //
        MemberDoc memberDoc = new MemberDoc(membership);
        memberRepository.save(memberDoc);
    }

    @Override
    public Membership retrieve(String id) {
        //
        Optional<MemberDoc> memberDoc = memberRepository.findById(id);
        return memberDoc.map(MemberDoc::toDomain).orElse(null);
    }

    @Override
    public List<Membership> retrieveAll(Offset offset) {
        //
        Pageable pageable = createPageable(offset);
        Page<MemberDoc> page = memberRepository.findAll(pageable);
        offset.setTotalCount(page.getTotalElements());
        List<MemberDoc> memberDocList = page.getContent();
        return MemberDoc.toDomains(memberDocList);
    }

    @Override
    public void update(Membership membership) {
        //
        MemberDoc memberDoc = new MemberDoc(membership);
       // memberRepository.save(memberDoc);
    }

    @Override
    public void delete(Membership membership) {
        //
        memberRepository.deleteById(membership.getId());
    }

    @Override
    public void delete(String id) {
        //
        memberRepository.deleteById(id);
    }

    private Pageable createPageable(Offset offset){
        //
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return (Pageable) PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return (Pageable) PageRequest.of(offset.page(), offset.limit());
        }
    }
}
