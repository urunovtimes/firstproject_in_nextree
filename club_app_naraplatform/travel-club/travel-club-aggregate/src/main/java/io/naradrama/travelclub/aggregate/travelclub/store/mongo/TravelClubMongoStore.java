package io.naradrama.travelclub.aggregate.travelclub.store.mongo;

import io.naradrama.prologue.domain.Offset;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import io.naradrama.travelclub.aggregate.travelclub.store.TravelClubStore;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.document.TravelClubDoc;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.repository.TravelClubRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TravelClubMongoStore implements TravelClubStore {
    //
    private final TravelClubRepository travelClubRepository;

    public TravelClubMongoStore(TravelClubRepository travelClubRepository) {
        this.travelClubRepository = travelClubRepository;
    }

    @Override
    public void create(TravelClub travelClub) {
        //
        TravelClubDoc travelClubDoc = new TravelClubDoc(travelClub);
        travelClubRepository.save(travelClubDoc);
    }

    @Override
    public TravelClub retrieve(String id) {
        //
        Optional<TravelClubDoc> travelClubDoc = travelClubRepository.findById(id);
        return travelClubDoc.map(TravelClubDoc::toDomain).orElse(null);
    }

    @Override
    public void update(TravelClub travelClub) {
      //
      TravelClubDoc travelClubDoc = new TravelClubDoc(travelClub);
      travelClubRepository.save(travelClubDoc);
    }

    @Override
    public void delete(TravelClub travelClub) {
        travelClubRepository.deleteById(travelClub.getId());
    }

    @Override
    public void delete(String id) {
        travelClubRepository.deleteById(id);
    }

    @Override
    public List<TravelClub> retrieveAll(Offset offset) {
        Pageable pageable = createPageable(offset);
        Page<TravelClubDoc> page = travelClubRepository.findAll(pageable);
        offset.setTotalCount(page.getTotalElements());
        List<TravelClubDoc> travelClubDocs = page.getContent();
        return TravelClubDoc.toDomain(travelClubDocs);
    }


    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
