package io.naradrama.travelclub.aggregate.travelclub.domain.logic;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.Offset;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.travelclub.aggregate.travelclub.api.command.command.MemberCommand;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.sdo.MemberCdo;
import io.naradrama.travelclub.aggregate.travelclub.domain.event.MemberEvent;
import io.naradrama.travelclub.aggregate.travelclub.domain.event.ServiceEventBuilder;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.MemberMongoStore;
import io.naraplatform.daysman.mediator.spec.EventStreamService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MemberLogic {
    //
    private final TravelClubLogic travelClubLogic;
    private final MemberMongoStore memberStore;
    private final EventStreamService streamService;

    public MemberLogic(TravelClubLogic travelClubLogic, MemberMongoStore memberStore, EventStreamService streamService) {
        this.travelClubLogic = travelClubLogic;
        this.memberStore = memberStore;
        this.streamService = streamService;
    }

   public MemberCommand routeCommand(MemberCommand command) throws NoSuchFieldException {
        //
       switch (command.getCqrsBaseCommandType()) {
           case Register:
               String entityId = this.registerMember(command.getMemberCdo(), command.genCommandIdentity());
               command.setCommandResponse(new CommandResponse(entityId));
               break;
           case Modify:
               this.modifyMember(command.getId(), command.getValueList(), command.genCommandIdentity());
               command.setCommandResponse(new CommandResponse(command.getId()));
               break;
           case Remove:
               this.removeMember(command.getId(), command.genCommandIdentity());
               command.setCommandResponse(new CommandResponse(command.getId()));
               break;
           default:
               command.setFailureMessage(new FailureMessage(new Throwable("CommandType must Registred, Modify or Remove")));

       }
       return command;
   }

   public String registerMember(MemberCdo memberCdo, CommandIdentity commandIdentity)  {
        //
       Membership membership = new Membership(memberCdo);
       memberStore.create(membership);
       MemberEvent memberEvent = MemberEvent.newMemberRegisterEvent(membership, commandIdentity);
       streamService.produce(ServiceEventBuilder.newEvent(memberEvent));
       return membership.getMemberId();
   }

   public Membership findMember(String memberId) throws NoSuchFieldException {
        //
       Membership membership = memberStore.retrieve(memberId);
       if(membership==null){
           throw new NoSuchFieldException("No Member id: " + memberId);
       }
       return membership;
   }

   public List<Membership> findAllMember(Offset offset){
        //
       return memberStore.retrieveAll(offset);
   }

   public void modifyMember(String id, NameValueList nameValueList, CommandIdentity commandIdentity) throws NoSuchFieldException {
        //
       Membership membership = findMember(id);
       membership.modifyValues(nameValueList);
       memberStore.update(membership);
       MemberEvent memberEvent = MemberEvent.newMemberEventModifiedEvent(id, nameValueList, commandIdentity);
       streamService.produce(ServiceEventBuilder.newEvent(memberEvent));
   }

   public void removeMember(String id, CommandIdentity commandIdentity) throws NoSuchFieldException {
        //
       Membership membership = findMember(id);
       memberStore.delete(membership);
       MemberEvent memberEvent = MemberEvent.newMemberRemovedEvent(id, commandIdentity);
       streamService.produce(ServiceEventBuilder.newEvent(memberEvent));
   }

   public void handleEventForProjection(MemberEvent memberEvent) {
        //
       switch (memberEvent.getCqrsDataEventType()){
           case Registered:
               memberStore.create(memberEvent.getMembership());
               break;
           case Modified:
               Membership membership = memberStore.retrieve(memberEvent.getMemberId());
               membership.modifyValues(memberEvent.getNameValues());
               memberStore.update(membership);
               break;
           case Removed:
               memberStore.delete(memberEvent.getMemberId());
               break;
       }
   }




}
