package io.naradrama.travelclub.aggregate.travelclub.api.query.rest;

import io.naradrama.prologue.util.query.DocQueryRequest;
import io.naradrama.travelclub.aggregate.travelclub.api.query.facade.MemberFacade;
import io.naradrama.travelclub.aggregate.travelclub.api.query.query.MemberDynamicQuery;
import io.naradrama.travelclub.aggregate.travelclub.api.query.query.MemberQuery;
import io.naradrama.travelclub.aggregate.travelclub.api.query.query.MembersDynamicQuery;
import io.naradrama.travelclub.aggregate.travelclub.store.MemberClubStore;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.document.MemberDoc;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aggregate/membership/query")
public class MemberQueryResource implements MemberFacade {
    //
    private final MemberClubStore memberClubStore;
    private final DocQueryRequest<MemberDoc> request;

    public MemberQueryResource(MemberClubStore memberClubStore, MongoTemplate mongoTemplate){
        //
        this.memberClubStore = memberClubStore;
        this.request = new DocQueryRequest<>(mongoTemplate);
    }


    @Override
    @PostMapping("/")
    public MemberQuery execute(@RequestBody MemberQuery memberQuery) {
        //
        memberQuery.execute(memberClubStore);
        return memberQuery;
    }

    @Override
    public MemberDynamicQuery execute(@RequestBody MemberDynamicQuery memberDynamicQuery) {
        //
        memberDynamicQuery.execute(request);
        return memberDynamicQuery;
    }

    @Override
    public MembersDynamicQuery execute(@RequestBody MembersDynamicQuery membersDynamicQuery) {
        //
        membersDynamicQuery.execute(request);
        return membersDynamicQuery;
    }
}
