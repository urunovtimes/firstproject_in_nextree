package io.naradrama.travelclub.aggregate.travelclub.domain.entity.sdo;

import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class TravelClubCdo implements JsonSerializable {

    private Membership membership;

    public TravelClubCdo(Membership memberCdo) {
        //
        this.membership = memberCdo;
    }

    public static TravelClubCdo sample(){
        //
        TravelClubCdo travelClubCdo = new TravelClubCdo(Membership.sample());
        return travelClubCdo;
    }
}
