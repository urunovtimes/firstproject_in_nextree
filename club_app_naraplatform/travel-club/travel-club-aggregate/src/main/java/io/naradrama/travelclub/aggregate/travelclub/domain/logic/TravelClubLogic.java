package io.naradrama.travelclub.aggregate.travelclub.domain.logic;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.Offset;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.util.exception.NaraException;
import io.naradrama.travelclub.aggregate.travelclub.api.command.command.TravelClubCommand;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.sdo.TravelClubCdo;
import io.naradrama.travelclub.aggregate.travelclub.domain.event.ServiceEventBuilder;
import io.naradrama.travelclub.aggregate.travelclub.domain.event.TravelClubEvent;
import io.naradrama.travelclub.aggregate.travelclub.store.TravelClubStore;
import io.naraplatform.daysman.mediator.spec.EventStreamService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class TravelClubLogic {

    private final TravelClubStore travelClubStore;
    private final EventStreamService eventStreamService;

    public TravelClubCommand routeCommand(TravelClubCommand command){
        //
        switch (command.getCqrsBaseCommandType()){
            case Register:
                String entityId = this.registerTravelClub(command.getTravelClubCdo(), command.genCommandIdentity());
                command.setCommandResponse(new CommandResponse(entityId));
                break;
            case Modify:
                this.modifyTravelClub(command.getClubId(), command.getNameValueList(),command.genCommandIdentity());
                command.setCommandResponse(new CommandResponse(command.getClubId()));
                break;
            case Remove:
                this.removeTravelClub(command.getClubId(), command.genCommandIdentity());
                command.setCommandResponse(new CommandResponse(command.getClubId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove.")));
        }

        return command;
    }

    public String registerTravelClub(TravelClubCdo travelClubCdo, CommandIdentity commandIdentity){
        //
        TravelClub travelClub = new TravelClub(travelClubCdo);
        travelClubStore.create(travelClub);
        TravelClubEvent clubEvent = TravelClubEvent.newTravelClubRegisterEvent(travelClub, commandIdentity);
        eventStreamService.produce(ServiceEventBuilder.newEvent(clubEvent));
        return clubEvent.getClubId();
    }

    public TravelClub findTravelClub(String clubId) {
        //
        TravelClub club = travelClubStore.retrieve(clubId);
        if(club == null){
            throw new NaraException("cannot_find_travelclub_by_id", clubId);
        }
        return club;
    }

    public List<TravelClub> findAllTravelClub(Offset offset){
        //
        return travelClubStore.retrieveAll(offset);
    }

    public void modifyTravelClub(String clubId, NameValueList nameValueList, CommandIdentity commandIdentity){
        //
        TravelClub club = findTravelClub(clubId);
        club.modifyValues(nameValueList);
        travelClubStore.update(club);
        TravelClubEvent clubEvent = TravelClubEvent.newTravelClubModifiedEvent(clubId, nameValueList, commandIdentity);
        eventStreamService.produce(ServiceEventBuilder.newEvent(clubEvent));
    }

    public void modifyTravelClub(TravelClub travelClub, CommandIdentity commandIdentity){
        //Check existence
        TravelClub foundTravelClub = findTravelClub(travelClub.getId());
        travelClubStore.update(travelClub);
        TravelClubEvent travelClubEvent = TravelClubEvent.newTravelClubRegisterEvent(travelClub, commandIdentity);
        eventStreamService.produce(ServiceEventBuilder.newEvent(travelClubEvent));
    }

    public void removeTravelClub(String clubId, CommandIdentity commandIdentity) {
        //
        TravelClub travelClub = findTravelClub(clubId);
        travelClubStore.delete(travelClub);
        TravelClubEvent travelClubEvent = TravelClubEvent.newTravelClubRemoveEvent(travelClub.getId(), commandIdentity);
        eventStreamService.produce(ServiceEventBuilder.newEvent(travelClubEvent));
    }

    public void handleEventForProjection(TravelClubEvent clubEvent){
        switch (clubEvent.getCqrsDataEventType()){
            case Registered:
                travelClubStore.create(clubEvent.getTravelClub());
                break;
            case Modified:
                TravelClub travelClub = travelClubStore.retrieve(clubEvent.getClubId());
                travelClub.modifyValues(clubEvent.getNameValueList());
                travelClubStore.update(travelClub);
                break;
            case Removed:
                travelClubStore.delete(clubEvent.getTravelClub());
                break;
        }
    }
}
