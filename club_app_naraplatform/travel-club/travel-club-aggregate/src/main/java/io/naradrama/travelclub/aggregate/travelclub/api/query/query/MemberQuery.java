package io.naradrama.travelclub.aggregate.travelclub.api.query.query;

import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import io.naradrama.travelclub.aggregate.travelclub.store.MemberClubStore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MemberQuery extends CqrsBaseQuery<Membership> {
    //
    private String id;

    public void execute(MemberClubStore memberClubStore){
        //
        setQueryResult(memberClubStore.retrieve(id));
    }
}
