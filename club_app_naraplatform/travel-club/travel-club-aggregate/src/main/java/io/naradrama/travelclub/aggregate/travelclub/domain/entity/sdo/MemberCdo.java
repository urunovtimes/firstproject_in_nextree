package io.naradrama.travelclub.aggregate.travelclub.domain.entity.sdo;


import io.naradrama.prologue.util.json.JsonSerializable;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.vo.RoleInClub;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MemberCdo implements JsonSerializable {
    //
    private String clubId;
    private String memberId;
    private RoleInClub role;
    private String joinDate;
    private TravelClub travelClub;

    public MemberCdo(TravelClub travelClub){
        //
        this.travelClub = travelClub;
        this.role = RoleInClub.President;
    }
}
