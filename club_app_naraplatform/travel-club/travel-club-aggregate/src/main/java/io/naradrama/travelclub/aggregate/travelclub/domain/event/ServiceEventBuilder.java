package io.naradrama.travelclub.aggregate.travelclub.domain.event;

import io.naradrama.prologue.domain.cqrs.Requester;
import io.naradrama.prologue.domain.cqrs.RequesterType;
import io.naradrama.prologue.domain.cqrs.broker.StreamEventMessage;
import io.naradrama.prologue.domain.cqrs.event.CqrsEvent;

public interface ServiceEventBuilder {
    //
    String serviceName = "io.naradrama.travelclub";
    static StreamEventMessage newEvent(CqrsEvent event) {
        //
        if(event.getRequester() == null){
            RequesterType requesterType = RequesterType.AppFront;
            event.setRequester(new Requester(event.getCommandId(), requesterType, serviceName));
        }
        return new StreamEventMessage(event);
    }
}
