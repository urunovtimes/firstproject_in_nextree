package io.naradrama.travelclub.aggregate.travelclub.store.mongo.repository;

import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import io.naradrama.travelclub.aggregate.travelclub.store.mongo.document.MemberDoc;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends MongoRepository<MemberDoc, String> {
}
