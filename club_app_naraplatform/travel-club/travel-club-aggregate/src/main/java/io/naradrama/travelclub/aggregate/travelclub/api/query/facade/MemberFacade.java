package io.naradrama.travelclub.aggregate.travelclub.api.query.facade;

import io.naradrama.travelclub.aggregate.travelclub.api.query.query.MemberDynamicQuery;
import io.naradrama.travelclub.aggregate.travelclub.api.query.query.MemberQuery;
import io.naradrama.travelclub.aggregate.travelclub.api.query.query.MembersDynamicQuery;

public interface MemberFacade {
    //
    MemberQuery execute(MemberQuery memberQuery);
    MemberDynamicQuery execute(MemberDynamicQuery memberDynamicQuery);
    MembersDynamicQuery execute(MembersDynamicQuery membersDynamicQuery);
}
