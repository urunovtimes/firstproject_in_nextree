package io.naradrama.travelclub.aggregate.travelclub.domain.event;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.CommandIdentity;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEvent;
import io.naradrama.prologue.domain.cqrs.event.CqrsDataEventType;
import io.naradrama.prologue.util.json.JsonUtil;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberEvent extends CqrsDataEvent {
    //
    private Membership membership;
    private String memberId;
    private NameValueList nameValues;



    public MemberEvent(CqrsDataEventType cqrsDataEventType, CommandIdentity commandIdentity) {
        super(cqrsDataEventType, commandIdentity);
    }

    public static MemberEvent newMemberRegisterEvent(Membership membership, CommandIdentity commandIdentity) {
        //
        MemberEvent event = new MemberEvent(CqrsDataEventType.Registered, commandIdentity);
        event.setMembership(membership);
        return event;
    }

    public static MemberEvent newMemberEventModifiedEvent(String memberId, NameValueList nameValues, CommandIdentity commandIdentity){
        //
        MemberEvent event = new MemberEvent(CqrsDataEventType.Modified, commandIdentity);
        event.setMemberId(memberId);
        event.setNameValues(nameValues);
        return event;
    }

    public static MemberEvent newMemberEventModifiedEvent(Membership membership, CommandIdentity commandIdentity){
        //
        MemberEvent event = new MemberEvent(CqrsDataEventType.Modified, commandIdentity);
        event.setMemberId(membership.getMemberId());
        event.setMembership(membership);
        return event;
    }

    public static MemberEvent newMemberRemovedEvent(String memberId, CommandIdentity commandIdentity) {
        //
        MemberEvent event = new MemberEvent(CqrsDataEventType.Removed, commandIdentity);
        event.setMemberId(memberId);
        return event;
    }

    public String toString() {
        //
        return toJson();
    }

    public static MemberEvent fromJson(String json) {
        //
        return JsonUtil.fromJson(json, MemberEvent.class);
    }
}
