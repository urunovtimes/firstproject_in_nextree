package io.naradrama.travelclub.aggregate.travelclub.store.mongo.document;

import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.vo.RoleInClub;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "MEMBERSHIP")
public class MemberDoc extends DomainEntityDoc {
    //
    private String clubId;
    private String memberId;
    private RoleInClub role;
    private String joinDate;

    public MemberDoc (Membership membership){
        //
        super(String.valueOf(membership));
        BeanUtils.copyProperties(membership, this);
    }

    public Membership toDomain(){
        //
        Membership membership = new Membership(getMemberId());
        BeanUtils.copyProperties(this, membership);
        return membership;
    }

    public static List<Membership> toDomains(List<MemberDoc> memberDocs){
        //
        return memberDocs.stream().map(MemberDoc::toDomain).collect(Collectors.toList());
    }

    public String toString(){
        //
        return toJson();
    }

    public static MemberDoc sample(){
        //
        return new MemberDoc(Membership.sample());
    }

    public static void main(String []args){
        //
        System.out.println(sample());
    }
}
