package io.naradrama.travelclub.constant;

public class TravelClubExceptionMessage {
    //
    public static final String resourceNotFoundException = "resource_cannot_found_it";
    public static final String invalidEmail = "invalid_email";
    public static final String noSuchFieldException = "no_such_field_exception";
}
