package io.naradrama.travelclub.aggregate.travelclub.api.command.command;

import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.sdo.TravelClubCdo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TravelClubCommand extends CqrsBaseCommand {
    //
    private TravelClubCdo travelClubCdo;
    private String clubId;
    private NameValueList nameValueList;

    protected TravelClubCommand(CqrsBaseCommandType type){
        //
        super(type);
    }
    public  static TravelClubCommand newRegisterTravelClubCommand(TravelClubCdo travelClubCdo) {
        TravelClubCommand command = new TravelClubCommand(CqrsBaseCommandType.Register);
        command.setTravelClubCdo(travelClubCdo);
        return command;
    }

    public static TravelClubCommand newModifyTravelClubCommand(String clubId, NameValueList nameValueList){
        //
        TravelClubCommand command = new TravelClubCommand(CqrsBaseCommandType.Modify);
        command.setClubId(clubId);
        command.setNameValueList(nameValueList);
        return command;
    }

    public static TravelClubCommand newRemoveTravelClubCommand(String clubId){
        //
        TravelClubCommand command = new TravelClubCommand(CqrsBaseCommandType.Remove);
        command.setClubId(clubId);
        return command;
    }

    public String toString(){
        //
        return toJson();
    }

    public static TravelClubCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, TravelClubCommand.class);
    }
}
