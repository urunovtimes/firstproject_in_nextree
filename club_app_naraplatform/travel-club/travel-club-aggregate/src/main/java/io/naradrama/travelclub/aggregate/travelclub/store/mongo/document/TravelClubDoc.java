package io.naradrama.travelclub.aggregate.travelclub.store.mongo.document;

import io.naradrama.prologue.store.mongo.DomainEntityDoc;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import io.naradrama.travelclub.aggregate.travelclub.domain.entity.vo.RoleInClub;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "TRAVELCLUB")
public class TravelClubDoc extends DomainEntityDoc {

   // private DockerTravelClub travelClub;
    private RoleInClub roleInClub;
    private String name;
    private String intro;
    private long foundationTime;


    public TravelClubDoc(TravelClub travelClub) {
       super(String.valueOf(travelClub));
       BeanUtils.copyProperties(travelClub, this);
    }

    public TravelClub toDomain(){
        //
        TravelClub travelClub = new TravelClub(id);
        BeanUtils.copyProperties(this, travelClub);
        return travelClub;
    }

    public static List<TravelClub> toDomain(List<TravelClubDoc> travelClubDoc) {
        //
        return travelClubDoc.stream().map(TravelClubDoc::toDomain).collect(Collectors.toList());
    }

    public String toString(){
        //
        return toJson();
    }
}
