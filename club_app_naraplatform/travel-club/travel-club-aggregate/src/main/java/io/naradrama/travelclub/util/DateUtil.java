package io.naradrama.travelclub.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static Date today() {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
       // System.out.println(formatter.format(date));
        return date;
    }
}
