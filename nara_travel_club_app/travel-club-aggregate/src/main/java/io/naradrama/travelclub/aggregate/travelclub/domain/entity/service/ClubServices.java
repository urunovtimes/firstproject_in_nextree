package io.naradrama.travelclub.aggregate.travelclub.service;


import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;

import java.util.List;

public interface ClubServices {


    // Create Travel club
    TravelClub createTravelClub(TravelClub travelClub);

    // Read All Club information
    List<TravelClub> getAllClub();

    // Update TravelClub Info
    TravelClub updateTravelClub(TravelClub travelClub);

    // Delete TravelClub
    void deleteTravelClub(String id);

    // By find only id
    TravelClub getTravelClubById(String id);



}
