package io.naradrama.travelclub.aggregate.travelclub.repository;

import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClubRepository extends MongoRepository<TravelClub, String> {
}
