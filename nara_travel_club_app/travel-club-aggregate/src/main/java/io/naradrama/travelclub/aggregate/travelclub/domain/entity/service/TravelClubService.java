package io.naradrama.travelclub.aggregate.travelclub.service;

import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import io.naradrama.travelclub.aggregate.travelclub.exception.ResourceNotFoundException;
import io.naradrama.travelclub.aggregate.travelclub.repository.ClubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TravelClubService implements ClubServices{

    @Autowired
    private ClubRepository clubRepository;

    @Override
    public TravelClub createTravelClub(TravelClub travelClub) {
        return clubRepository.save(travelClub);
    }

    @Override
    public List<TravelClub> getAllClub() {
        return this.clubRepository.findAll();
    }

    @Override
    public TravelClub updateTravelClub(TravelClub travelClub) {

        Optional<TravelClub> travelClubDb = this.clubRepository.findById(travelClub.getId());

        if(travelClubDb.isPresent()) {
            TravelClub clubUpdate = travelClubDb.get();
            clubUpdate.setId(travelClub.getId());
            clubUpdate.setIntro(travelClub.getIntro());
            clubUpdate.setName(travelClub.getName());
            clubUpdate.setFoundationTime(travelClub.getFoundationTime());

            return clubUpdate;
        } else {
         throw new ResourceNotFoundException("Club could not found with id: " + travelClub.getId());
        }
    }

    @Override
    public void deleteTravelClub(String id) {
        Optional<TravelClub> travelClubDB = this.clubRepository.findById(id);

        if (travelClubDB.isPresent()){
            this.clubRepository.delete(travelClubDB.get());
        } else {
            throw new ResourceNotFoundException("Club not found with id: " + id);
        }
    }

    @Override
    public TravelClub getTravelClubById(String id) {

        Optional<TravelClub> travelClubDB = this.clubRepository.findById(id);

        if(travelClubDB.isPresent()){
            return travelClubDB.get();
        } else{
            throw new ResourceNotFoundException("Club not found with id: "+ id);
        }
    }




}
