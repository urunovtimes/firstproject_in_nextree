package io.naradrama.travelclub.aggregate.travelclub.controller.api;

import io.naradrama.travelclub.aggregate.travelclub.domain.entity.TravelClub;
import io.naradrama.travelclub.aggregate.travelclub.service.TravelClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/")
public class TravelClubAPI {

     @Autowired
     private TravelClubService travelClubService;

     @GetMapping("/clubs")
     public ResponseEntity <List <TravelClub>> getAllClubs(){
         return ResponseEntity.ok().body(travelClubService.getAllClub());
     }

     @GetMapping("/clubs/{id}")
     public ResponseEntity<TravelClub> getTravelClubId(@PathVariable String id){
         return ResponseEntity.ok().body(travelClubService.getTravelClubById(id));
     }

     @PostMapping("/clubs")
     public ResponseEntity<TravelClub> createTravelClub(@RequestBody TravelClub travelClub){
         return ResponseEntity.ok().body(travelClubService.createTravelClub(travelClub));
     }

     @PutMapping("/clubs/{id}")
     public ResponseEntity <TravelClub> updateTravelClub(@PathVariable String id, @RequestBody TravelClub travelClub) {
         travelClub.setId(id);
         return ResponseEntity.ok().body(this.travelClubService.updateTravelClub(travelClub));
     }

     @DeleteMapping("/clubs/{id}")
     public HttpStatus deleteClub(@PathVariable String id){
         this.travelClubService.deleteTravelClub(id);

         return HttpStatus.OK;
     }




}
