package io.naradrama.travelclub.aggregate.travelclub.repository;

import io.naradrama.travelclub.aggregate.travelclub.domain.entity.Membership;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends MongoRepository<Membership, String> {
}
