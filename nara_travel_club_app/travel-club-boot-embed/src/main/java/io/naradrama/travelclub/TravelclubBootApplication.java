/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.travelclub;

import springfox.documentation.swagger2.annotations.EnableSwagger2;
import io.naraplatform.envoy.config.NaraDramaApplication;
import io.naraplatform.daysman.daysboy.config.EnableDaysman;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import java.lang.String;
import org.springframework.boot.SpringApplication;

@EnableSwagger2
@NaraDramaApplication
@EnableDaysman
@SpringBootApplication(scanBasePackages = { "io.naradrama.travelclub" })
@EnableMongoRepositories(basePackages = { "io.naradrama.travelclub", "io.naraplatform.daysman.daysboy.store.mongo" })
@EntityScan(basePackages = { "io.naradrama.travelclub", "io.naraplatform.daysman.daysboy.store" })
public class TravelclubBootApplication {
    /* Autogen by nara studio */

    public static void main(String[] args) {
        /* Autogen by nara studio */
        SpringApplication.run(TravelclubBootApplication.class, args);
    }
}
